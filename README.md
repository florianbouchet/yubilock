# Yubilock

## Description

Lock your computer when your Yubikey is unplugged.

## Usage

### Compilation

```bash
make
```

### Run

Be sure you already compiled the source files, and then run:

```bash
./yubilock
```

### Remove all generated files

```bash
make .PHONY
```