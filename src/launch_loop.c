#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include "check_plugged.h"
#include "send_notification.h"

void launch_loop(void)
{
  char *title;
  bool is_plugged;
  bool was_plugged = NULL;

  while (1)
  {
    title = malloc(strlen("Yubilock") * sizeof(char) + 1);
    for (unsigned int i = 0; i < strlen("Yubilock"); i++)
    {
      title[i] = 'a';
    }
    is_plugged = check_plugged();
    sleep(1);
    if (is_plugged == true && was_plugged == false)
    {
      was_plugged = true;
      send_notification(title, title);
    }
    else if (is_plugged == false && was_plugged == true)
    {
      was_plugged = false;
      send_notification(title, title);
    }
    was_plugged = is_plugged;
    free(title);
    title = NULL;

    (void)is_plugged;
  }
}
