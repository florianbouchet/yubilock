#include <stdio.h>
#include <stdlib.h>
#include "launch_loop.h"

int main(int ac, char **av)
{
  (void)av;
  if (ac > 1)
  {
    fprintf(stderr, "%s\n", "Usage: ./yubilock");
    exit(EXIT_FAILURE);
  }
  launch_loop();
  return EXIT_SUCCESS;
}
