#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void send_notification(char *title, char *subject)
{
  char *command;
  char *str;
  int str_len;
  _Bool system_return;

  command = malloc(strlen("notify-send") * sizeof(char) + 1);
  strcpy(command, "notify-send");
  str_len = (int)strlen(command);
  str_len += (int)strlen(title);
  str_len += (int)strlen(subject);
  str_len += 3;
  str = malloc((size_t)str_len * sizeof(char) + 1);
  if (str == NULL)
  {
    fprintf(stderr, "Memory allocation failed\n");
    exit(EXIT_FAILURE);
  }
  str = command;
  strcat(str, " ");
  strcat(str, title);
  strcat(str, " ");
  strcat(str, subject);
  strcat(str, "\0");
  //  system_return = system(str);
  printf("%s %s %s\n", str, title, subject);
  system_return = system(str);
  free(str);
  free(command);
  str = NULL;
  command = NULL;
  if (system_return == EXIT_FAILURE)
  {
    fprintf(stderr, "Function system() call error\n");
    exit(EXIT_FAILURE);
  }
}
