CC=gcc
CFLAGS=-W -Wextra -Wall -Winit-self -Wuninitialized -Wfloat-equal -Wundef -Wshadow -Wpointer-arith -Wcast-align -Wstrict-prototypes -Wstrict-overflow=5 -Wwrite-strings -Waggregate-return -Wcast-qual -Wswitch-default -Wswitch-enum -Wconversion -Wunreachable-code -Werror -ansi -pedantic -std=c99 -O2 -save-temps
LDFLAGS=
EXEC=yubilock
SRCPATH=src
SRC= $(wildcard $(SRCPATH)/*.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

main.o: hello.h

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean cleansi mrproper

clean:
	rm -rf $(SRCPATH)/*.o

cleansi: clean
	rm -rf *.s *.i

mrproper: clean
	rm -rf $(EXEC)
